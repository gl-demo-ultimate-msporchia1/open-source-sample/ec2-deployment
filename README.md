# ec2-deployment

ENV Variables needed:
- CI_AWS_CF_STACK_NAME - the name of the stack, the suggestion is to use a combination of a {custom-name}-{uniquevalue} (e.g. myapp-branch-commitsha)
- CI_AWS_CF_CREATE_STACK_FILE - the project's relative location of the AWS stack to create (in this sample, it's aws/sample-instance.yaml)
- AWS_ACCESS_KEY_ID (temporary, to be substituted by OIDC)
- AWS_DEFAULT_REGION (temporary)
- AWS_SECRET_ACCESS_KEY (temporary)
- CI_AWS_S3_PUSH_FILE - GitLab Cloud Deploy-compliant file path to push artifacts to s3, prepped for CodeDeploy
- CI_AWS_EC2_DEPLOYMENT_FILE - CodeDeploy-compliant file path to allow CodeDeploy to deploy our application to the instance (see sample at aws/application_deploy.json - more details here: https://docs.gitlab.com/ee/ci/cloud_deployment )


## possible improvements to the component:

1. Support CFN in Yaml format (currently, only JSON is supported by our Cloud Deploy script)
1. Make sure CAPABILITY_NAMED_IAM is specified
1. Cleanup environment when the pipeline fails (e.g. in a Merge Request, if the CFN is deployed the first time, it's not being updated)
1. Promote the use of OIDC
1. Make sure that the CFN Stack create arguments can be passed via variables
1. Facilitate/provide best-practices on how to differentiate deployment between environments
1. Reduce the amount of ENV Variables needed by leveraging best-practices, revert to the ENV Variables if customer wants freedom
1. Add a "Stop Environment" action to the Component to enable review Environments

Some of the modifications require a modification on the "Cloud Deploy" GitLab AWS Wrapper